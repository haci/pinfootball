package com.mobaxe.pinfootball.android;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.mobaxe.pinfootball.ActionResolver;
import com.mobaxe.pinfootball.PinFootball;

public class AndroidLauncher extends AndroidApplication implements ActionResolver {

    private static final String AD_UNIT_ID = "ca-app-pub-5727465082302443/6528153412";
    private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-5727465082302443/4540265810";
    private static final String TEST_DEVICE_ID = "8E452640BC83C672B070CDCA8AB9B06B";
    private AdView adView;
    private View gameView;
    private RelativeLayout layout;
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new PinFootball(this), config);

        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(AD_UNIT_ID);

        layout = new RelativeLayout(this);

        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        RelativeLayout.LayoutParams gameParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        gameParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        gameParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(TEST_DEVICE_ID).build();

        adView.loadAd(adRequest);

        gameView = initializeForView(new PinFootball(this), config);

        layout.addView(gameView, gameParams);
        layout.addView(adView, adParams);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(AD_UNIT_ID_INTERSTITIAL);

        showOrLoadInterstital();

        setContentView(layout);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adView != null) {
            adView.pause();
        }

    }

    /**
     * Called before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (adView != null) {
            adView.destroy();
        }
    }

    @Override
    public void showOrLoadInterstital() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (interstitialAd.isLoaded()) {
                        interstitialAd.show();

                    } else {
                        AdRequest interstitialRequest = new AdRequest.Builder().build();
                        interstitialAd.loadAd(interstitialRequest);

                    }
                }
            });
        } catch (Exception e) {
        }
    }

}
