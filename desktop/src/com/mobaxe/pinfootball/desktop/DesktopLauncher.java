package com.mobaxe.pinfootball.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mobaxe.pinfootball.PinFootball;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Pin Football";
		config.useGL30 = false;
		config.width = 480;
		config.height = 720;
		new LwjglApplication(new PinFootball(new ActionResolverDesktop()), config);
	}
}
