package com.mobaxe.pinfootball;

import com.badlogic.gdx.Screen;
import com.mobaxe.pinfootball.screens.GameScreen;
import com.mobaxe.pinfootball.screens.MainMenuScreen;

public enum MyScreens {

	MAIN_MENU_SCREEN {
		public Screen getScreenInstance() {
			return new MainMenuScreen(PinFootball.actionResolver);
		}
	},
	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
