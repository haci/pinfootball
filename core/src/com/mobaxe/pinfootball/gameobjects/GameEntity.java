package com.mobaxe.pinfootball.gameobjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.mobaxe.pinfootball.helpers.Box2DFactory;

public class GameEntity extends CircleShape {
	// Our game table
	private World world;
	private Body aBody;

	// Radius of ball
	float playerRadius = 1.1f;
	private EntityProperty entityProperty;
	private FixtureDef entityFixture;

	public GameEntity(World world, EntityProperty entityProperty) {
		this.world = world;
		this.entityProperty = entityProperty;

	}

	public void initEntity() {
		Shape entityShape = Box2DFactory.createCircleShape(playerRadius);
		// entityFixture = Box2DFactory.createFixture(entityShape, 0.1f, 2f,
		// 0.90f, false);
		// entityFixture.filter.groupIndex = 1;

		BodyType entityBodyType = BodyType.DynamicBody;

		Vector2 startPosition = new Vector2();
		switch (entityProperty) {
		case PLAYER_LP:
			entityFixture = Box2DFactory.createFixture(entityShape, 0.1f, 0.1f, 0.8f, false);
			entityFixture.filter.groupIndex = -1;
			startPosition.set(0, 0);
			break;
		default:
			startPosition.set(0, 0);
			playerRadius = 1;
			break;
		}

		aBody = Box2DFactory.createBody(world, entityBodyType, entityFixture, startPosition);
		aBody.setUserData(entityProperty);
	}

	public Body getaBody() {
		return aBody;
	}

	public float getPlayerRadius() {
		return playerRadius;
	}

	public void setEntityProperty(EntityProperty entityProperty) {
		this.entityProperty = entityProperty;
	}

	public EntityProperty getEntityProperty() {
		return entityProperty;
	}

	public void setaBody(Body aBody) {
		this.aBody = aBody;
	}
}
