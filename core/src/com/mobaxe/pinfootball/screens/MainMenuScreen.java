package com.mobaxe.pinfootball.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.pinfootball.ActionResolver;
import com.mobaxe.pinfootball.GameState;
import com.mobaxe.pinfootball.entities.CustomButton;
import com.mobaxe.pinfootball.helpers.AssetsLoader;
import com.mobaxe.pinfootball.utils.Constants;

public class MainMenuScreen implements Screen {

	private Stage stage;
	private Table table;
	private Table bgTable;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;

	private CustomButton startGameButton;

	private ActionResolver actionResolver;
	public int adCounter = Constants.INTERSTITIAL_FREQ - 1;
	public static boolean adIsShowed = false;

	public MainMenuScreen(ActionResolver actionResolver) {
		stage = new Stage(new StretchViewport(Constants.virtualWidth, Constants.virtualHeight));
		table = new Table();
		bgTable = new Table();
		this.actionResolver = actionResolver;

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
		if (GameScreen.gameState == GameState.GAMEOVER) {
			GameScreen.gameState = GameState.PAUSE;
			showInterstitial();
		}

	}

	public void showInterstitial() {
		// Ads every X gameovers!
		if (adCounter == Constants.INTERSTITIAL_FREQ) {
			if (!adIsShowed) {
				actionResolver.showOrLoadInterstital();
				adIsShowed = true;
				adCounter--;
			}
		} else if (adCounter == 0) {
			adCounter = Constants.INTERSTITIAL_FREQ;
		} else {
			adCounter--;
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		// Gdx.input.setCatchBackKey(true);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {
		table.setFillParent(true);
		bgTable.setFillParent(true);

		startGameButton = new CustomButton(CustomButton.START, true);
		table.add(startGameButton).pad(0, 0, 25, 0);
		table.row();

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);

		stage.addActor(bgTable);
		stage.addActor(table);
	}

	private void setBackgroundImage() {
		bgTexture = AssetsLoader.loadTexture("mainmenuimages/mainmenuBackground.png");

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(Constants.virtualWidth - bgSprite.getWidth() / 2);
		bgSprite.setY(Constants.virtualHeight - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
