package com.mobaxe.pinfootball.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.pinfootball.GameState;
import com.mobaxe.pinfootball.MyScreens;
import com.mobaxe.pinfootball.ScreenManager;
import com.mobaxe.pinfootball.gameobjects.EntityProperty;
import com.mobaxe.pinfootball.gameobjects.Player;
import com.mobaxe.pinfootball.helpers.AssetsLoader;
import com.mobaxe.pinfootball.helpers.Box2DFactory;
import com.mobaxe.pinfootball.utils.Constants;

public class GameScreen extends InputAdapter implements Screen {

	/* Use Box2DDebugRenderer, which is a model renderer for debug purposes */
	private Box2DDebugRenderer debugRenderer;
	private Player player;
	/* As always, we need a camera to be able to see the objects */
	private OrthographicCamera camera;
	/* Define a world to hold all bodies and simulate reactions between them */
	private World world;

	/*
	 * Used to define a mouse joint for a body. This point will track a
	 * specified world point.
	 */
	private MouseJoint mouseJoint;
	private MouseJointDef mouseJointDef;

	/* Store the position of the last touch or mouse click */
	private Vector3 touchPosition;
	@SuppressWarnings("unused")
	private Body rightGoalLine;
	@SuppressWarnings("unused")
	private Body leftGoalLine;
	@SuppressWarnings("unused")
	private Body rightUpLine;
	@SuppressWarnings("unused")
	private Body leftUpLine;
	@SuppressWarnings("unused")
	private Body leftLine;
	@SuppressWarnings("unused")
	private Body rightLine;
	@SuppressWarnings("unused")
	private Body cornerLineLeftUp;
	@SuppressWarnings("unused")
	private Body cornerLineLeftDown;
	@SuppressWarnings("unused")
	private Body cornerLineRightUp;
	@SuppressWarnings("unused")
	private Body cornerLineRightDown;
	private Body goalLineUp;
	@SuppressWarnings("unused")
	private Body goalLineDown;
	private Body invisibleWalls;
	private int oppositeScore;
	private int playerScore;
	private SpriteBatch batch;
	private boolean scored = false;
	private Sound hitWalls;
	private Sound goalSound;
	private Sound gamerSound;
	private Body clickedBody;
	private Body opsScore;
	private Body plyrScore;
	private Body dot1, dot2, dot3, dot4, dot5, dot6, dot7, dot8, dot9, dot10, dot11, dot12, dot13, dot14, dot15, dot16,
			dot17, dot18;
	private Sprite pin;
	private Sprite sprite;
	public static GameState gameState = GameState.PAUSE;
	private float spinTime;
	protected boolean gameover = false;
	private boolean controller = false;

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// debugRenderer.render(world, camera.combined);
		switch (gameState) {
		case RUNNING:
			gameRunning();
			spinBall();
			break;
		case GAMEOVER:
			gameOver();
			break;
		case PAUSE:
			gamePause();
			break;
		default:
			break;
		}

	}

	public void spinBall() {
		spinTime += Gdx.graphics.getDeltaTime();
		if (spinTime > -3f) {
			spinTime = 0f;
		}
		sprite.rotate(spinTime);
	}

	public void gameOver() {
		gameState = GameState.GAMEOVER;
		ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
		ScreenManager.getInstance().show(MyScreens.MAIN_MENU_SCREEN);

	}

	private void gamePause() {
		// RESTART AD
		if (MainMenuScreen.adIsShowed) {
			MainMenuScreen.adIsShowed = false;
		}
		gameover = false;

		gameState = GameState.RUNNING;

	}

	public void gameRunning() {
		checkCollision();

		goalLineUp.setUserData(loadSprites("scoreboard"));
		world.step(1f / 60f, 6, 2);

		// DRAW ALL THE SPRITES RELATED TO BODIES
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		drawGameObjects();
		batch.end();

	}

	public void drawGameObjects() {
		// STADIUM IMAGE
		if (invisibleWalls.getUserData() != null && invisibleWalls.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) invisibleWalls.getUserData();
			sprite.setPosition(invisibleWalls.getPosition().x - sprite.getWidth() / 2f, invisibleWalls.getPosition().y
					- sprite.getHeight() / 2);
			sprite.draw(batch);
		}
		// PLAYER IMAGE
		if (player.getaBody().getUserData() != null && player.getaBody().getUserData() instanceof Sprite) {
			sprite = (Sprite) player.getaBody().getUserData();
			sprite.setPosition(player.getaBody().getPosition().x - sprite.getWidth() / 2, player.getaBody()
					.getPosition().y - sprite.getHeight() / 2);
			sprite.draw(batch);
		}
		// OPPOSITE SCORE IMAGE
		if (opsScore.getUserData() != null && opsScore.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) opsScore.getUserData();
			sprite.setPosition(opsScore.getPosition().x - sprite.getWidth() + 10f,
					opsScore.getPosition().y - sprite.getHeight() + 4.5f);
			sprite.draw(batch);
		}
		// PLAYER SCORE IMAGE
		if (plyrScore.getUserData() != null && plyrScore.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) plyrScore.getUserData();
			sprite.setPosition(plyrScore.getPosition().x - sprite.getWidth() + 10f,
					plyrScore.getPosition().y - sprite.getHeight() - 1.3f);
			sprite.draw(batch);
		}
		// SCOREBOARD IMAGE
		if (goalLineUp.getUserData() != null && goalLineUp.getUserData() instanceof Sprite) {
			Sprite sprite = (Sprite) goalLineUp.getUserData();
			sprite.setPosition(goalLineUp.getPosition().x - sprite.getWidth() - 2.0f, goalLineUp.getPosition().y
					- sprite.getHeight() - 10.5f);
			sprite.draw(batch);
		}
		if (dot1.getUserData() != null && dot1.getUserData() instanceof Sprite) {
			if (dot1.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot1.getUserData();
				sprite.setPosition(dot1.getPosition().x - sprite.getWidth() / 2,
						dot1.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot2.getUserData() != null && dot2.getUserData() instanceof Sprite) {
			if (dot2.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot2.getUserData();
				sprite.setPosition(dot2.getPosition().x - sprite.getWidth() / 2,
						dot2.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot3.getUserData() != null && dot3.getUserData() instanceof Sprite) {
			if (dot3.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot3.getUserData();
				sprite.setPosition(dot3.getPosition().x - sprite.getWidth() / 2,
						dot3.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot4.getUserData() != null && dot4.getUserData() instanceof Sprite) {
			if (dot4.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot4.getUserData();
				sprite.setPosition(dot4.getPosition().x - sprite.getWidth() / 2,
						dot4.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot5.getUserData() != null && dot5.getUserData() instanceof Sprite) {
			if (dot5.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot5.getUserData();
				sprite.setPosition(dot5.getPosition().x - sprite.getWidth() / 2,
						dot5.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot6.getUserData() != null && dot6.getUserData() instanceof Sprite) {
			if (dot6.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot6.getUserData();
				sprite.setPosition(dot6.getPosition().x - sprite.getWidth() / 2,
						dot6.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot7.getUserData() != null && dot7.getUserData() instanceof Sprite) {
			if (dot7.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot7.getUserData();
				sprite.setPosition(dot7.getPosition().x - sprite.getWidth() / 2,
						dot7.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot8.getUserData() != null && dot8.getUserData() instanceof Sprite) {
			if (dot8.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot8.getUserData();
				sprite.setPosition(dot8.getPosition().x - sprite.getWidth() / 2,
						dot8.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot8.getUserData() != null && dot8.getUserData() instanceof Sprite) {
			if (dot8.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot8.getUserData();
				sprite.setPosition(dot8.getPosition().x - sprite.getWidth() / 2,
						dot8.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot9.getUserData() != null && dot9.getUserData() instanceof Sprite) {
			if (dot9.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot9.getUserData();
				sprite.setPosition(dot9.getPosition().x - sprite.getWidth() / 2,
						dot9.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot10.getUserData() != null && dot10.getUserData() instanceof Sprite) {
			if (dot10.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot10.getUserData();
				sprite.setPosition(dot10.getPosition().x - sprite.getWidth() / 2,
						dot10.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot11.getUserData() != null && dot11.getUserData() instanceof Sprite) {
			if (dot11.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot11.getUserData();
				sprite.setPosition(dot11.getPosition().x - sprite.getWidth() / 2,
						dot11.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot12.getUserData() != null && dot12.getUserData() instanceof Sprite) {
			if (dot12.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot12.getUserData();
				sprite.setPosition(dot12.getPosition().x - sprite.getWidth() / 2,
						dot12.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot13.getUserData() != null && dot13.getUserData() instanceof Sprite) {
			if (dot13.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot13.getUserData();
				sprite.setPosition(dot13.getPosition().x - sprite.getWidth() / 2,
						dot13.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot14.getUserData() != null && dot14.getUserData() instanceof Sprite) {
			if (dot14.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot14.getUserData();
				sprite.setPosition(dot14.getPosition().x - sprite.getWidth() / 2,
						dot14.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot15.getUserData() != null && dot15.getUserData() instanceof Sprite) {
			if (dot15.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot15.getUserData();
				sprite.setPosition(dot15.getPosition().x - sprite.getWidth() / 2,
						dot15.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot16.getUserData() != null && dot16.getUserData() instanceof Sprite) {
			if (dot16.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot16.getUserData();
				sprite.setPosition(dot16.getPosition().x - sprite.getWidth() / 2,
						dot16.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot17.getUserData() != null && dot17.getUserData() instanceof Sprite) {
			if (dot17.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot17.getUserData();
				sprite.setPosition(dot17.getPosition().x - sprite.getWidth() / 2,
						dot17.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}
		if (dot18.getUserData() != null && dot18.getUserData() instanceof Sprite) {
			if (dot18.getUserData().toString().equals(pin.toString())) {
				Sprite sprite = (Sprite) dot18.getUserData();
				sprite.setPosition(dot18.getPosition().x - sprite.getWidth() / 2,
						dot18.getPosition().y - sprite.getHeight() / 2);
				sprite.draw(batch);
			}
		}

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(this);
		// Gdx.input.setCatchBackKey(true);
		batch = new SpriteBatch();
		hitWalls = AssetsLoader.loadSound("sounds/hitWallSound.mp3");
		goalSound = AssetsLoader.loadSound("sounds/goalSound.mp3");
		gamerSound = AssetsLoader.loadSound("sounds/gamerSound.mp3");

		/*
		 * Create world with zero gravity vector and tell world that we want
		 * objects to sleep. This last value conserves CPU usage.
		 */
		world = new World(new Vector2(0, 0), true);
		createContactListener();
		/* Create renderer */
		debugRenderer = new Box2DDebugRenderer();

		/*
		 * Define camera viewport. Box2D uses meters internally so the camera
		 * must be defined also in meters. We set a desired width and adjust
		 * height to different resolutions.
		 */
		camera = new OrthographicCamera(Constants.WIDTH, Constants.height);

		/*
		 * Instantiate the vector that will be used to store click/touch
		 * positions.
		 */
		touchPosition = new Vector3();

		// Create the player1
		player = new Player(world);
		player.initEntity();
		player.getaBody().setUserData(loadSprites("player"));

		/* Create the walls */
		invisibleWalls = Box2DFactory.createInvisibleWalls(world);
		invisibleWalls.setUserData(loadSprites("stad"));

		opsScore = Box2DFactory.createScoreBoardOpposite(world);
		opsScore.setUserData(loadSprites("oppositeScore"));

		plyrScore = Box2DFactory.createScoreBoardPlayer(world);
		plyrScore.setUserData(loadSprites("playerScore"));

		leftLine = Box2DFactory.createLeftLine(world);
		rightGoalLine = Box2DFactory.createGoalLineRight(world);
		leftGoalLine = Box2DFactory.createGoalLineLeft(world);
		rightUpLine = Box2DFactory.createGoalLineRightUp(world);
		leftUpLine = Box2DFactory.createGoalLineLeftUp(world);
		rightLine = Box2DFactory.createRightLine(world);
		goalLineUp = Box2DFactory.createGoalLineDown(world);
		goalLineUp.setUserData(loadSprites("scoreboard"));
		goalLineDown = Box2DFactory.createGoalLineUp(world);

		pin = new Sprite(AssetsLoader.loadTexture("gameobjects/pin.png"));
		pin.setSize(2.8f, 2.8f);
		pin.setOrigin(pin.getWidth() / 2, pin.getHeight() / 2);

		dot1 = Box2DFactory.createPins(world, new Vector2(0, -14));
		dot1.setUserData(pin);
		dot2 = Box2DFactory.createPins(world, new Vector2(-6, -12));
		dot2.setUserData(pin);
		dot3 = Box2DFactory.createPins(world, new Vector2(-3, -9));
		dot3.setUserData(pin);
		dot4 = Box2DFactory.createPins(world, new Vector2(3, -9));
		dot4.setUserData(pin);
		dot5 = Box2DFactory.createPins(world, new Vector2(6, -12));
		dot5.setUserData(pin);
		dot6 = Box2DFactory.createPins(world, new Vector2(-6, -4));
		dot6.setUserData(pin);
		dot7 = Box2DFactory.createPins(world, new Vector2(6, -4));
		dot7.setUserData(pin);
		dot8 = Box2DFactory.createPins(world, new Vector2(-3, 0));
		dot8.setUserData(pin);
		dot9 = Box2DFactory.createPins(world, new Vector2(0, -2));
		dot9.setUserData(pin);
		dot10 = Box2DFactory.createPins(world, new Vector2(3, 0));
		dot10.setUserData(pin);
		dot11 = Box2DFactory.createPins(world, new Vector2(0, 2));
		dot11.setUserData(pin);
		dot12 = Box2DFactory.createPins(world, new Vector2(0, 14));
		dot12.setUserData(pin);
		dot18 = Box2DFactory.createPins(world, new Vector2(-3, 9));
		dot18.setUserData(pin);
		dot17 = Box2DFactory.createPins(world, new Vector2(3, 9));
		dot17.setUserData(pin);
		dot16 = Box2DFactory.createPins(world, new Vector2(6, 12));
		dot16.setUserData(pin);
		dot15 = Box2DFactory.createPins(world, new Vector2(-6, 4));
		dot15.setUserData(pin);
		dot14 = Box2DFactory.createPins(world, new Vector2(6, 4));
		dot14.setUserData(pin);
		dot13 = Box2DFactory.createPins(world, new Vector2(-6, 12));
		dot13.setUserData(pin);

		/* Define the mouse joint. We use walls as the first body of the joint */
		createMouseJointDefinition(invisibleWalls);

	}

	private Sprite loadSprites(String spriteName) {
		if (oppositeScore == 3 || playerScore == 3) {
			gameover = true;
			Timer.schedule(new Task() {
				@Override
				public void run() {
					oppositeScore = 0;
					playerScore = 0;
					gameState = GameState.GAMEOVER;
					gameover = false;
				}
			}, 3f);
		}
		if (gameover == false) {
			if (spriteName.equals("player")) {
				Sprite imageSprites = new Sprite(AssetsLoader.loadTexture("gameobjects/player.png"));
				imageSprites.setSize(2.1f, 2.1f);
				imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
				return imageSprites;
			}
		}

		if (spriteName.equals("stad")) {
			Sprite imageSprites = new Sprite(AssetsLoader.loadTexture("gameobjects/stad.png"));
			imageSprites.setSize(22f, 32f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		if (spriteName.equals("oppositeScore")) {
			Sprite imageSprites = null;
			if (oppositeScore == 0) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/zero.png"));
			}
			if (oppositeScore == 1) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/one.png"));
			}
			if (oppositeScore == 2) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/two.png"));
			}
			if (oppositeScore == 3) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/three.png"));
			}
			imageSprites.setSize(1.5f, 2.4f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		if (spriteName.equals("playerScore")) {
			Sprite imageSprites = null;
			if (playerScore == 0) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/zero.png"));
			}
			if (playerScore == 1) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/one.png"));
			}
			if (playerScore == 2) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/two.png"));
			}
			if (playerScore == 3) {
				imageSprites = new Sprite(AssetsLoader.loadTexture("scoreimages/three.png"));
			}
			imageSprites.setSize(1.5f, 2.4f);
			imageSprites.setOrigin(imageSprites.getWidth() / 2, imageSprites.getHeight() / 2);
			return imageSprites;
		}
		return null;
	}

	@Override
	public void dispose() {
		debugRenderer.dispose();
		world.dispose();
		hitWalls.dispose();
		goalSound.dispose();
		gamerSound.dispose();
	}

	public void createContactListener() {
		world.setContactListener(new ContactListener() {

			@Override
			public void beginContact(Contact contact) {
				if (contact.getFixtureB().getBody().getUserData() == player.getaBody().getUserData()) {
					gamerSound.play();
				}
				if (contact.getFixtureA().getBody().getUserData() == player.getaBody().getUserData()) {
					hitWalls.play();
					player.getaBody().setLinearDamping(3.5f);
					spinTime = -4;
				}
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {

			}

		});
	}

	/**
	 * Creates the MouseJoint definition.
	 * 
	 * @param body
	 *            First body of the joint (i.e. ground, walls, etc.)
	 */
	private void createMouseJointDefinition(Body body) {
		mouseJointDef = new MouseJointDef();
		mouseJointDef.bodyA = body;
		mouseJointDef.collideConnected = true;
		mouseJointDef.maxForce = 5000f;
		mouseJointDef.dampingRatio = 0;
		mouseJointDef.frequencyHz = 100;

	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		/*
		 * Define a new QueryCallback. This callback will be used in
		 * world.QueryAABB method.
		 */
		QueryCallback queryCallback = new QueryCallback() {
			@Override
			public boolean reportFixture(Fixture fixture) {
				boolean testResult;

				/*
				 * If the hit point is inside the fixture of the body, create a
				 * new MouseJoint.
				 */
				if (testResult = fixture.testPoint(touchPosition.x, touchPosition.y)) {
					mouseJointDef.bodyB = fixture.getBody();
					if (fixture.getBody().getUserData() != null) {
						clickedBody = fixture.getBody();
						if (fixture.getBody().getUserData() instanceof EntityProperty) {
							EntityProperty touchedEntityProp = (EntityProperty) fixture.getBody().getUserData();
							if (touchedEntityProp != EntityProperty.PLAYER_LP)
								return false;
							controller = true;
						}
					}
					mouseJointDef.target.set(player.getaBody().getPosition().x, player.getaBody().getPosition().y);
					mouseJoint = (MouseJoint) world.createJoint(mouseJointDef);

				}

				return testResult;
			}
		};

		/* Translate camera point to world point */
		camera.unproject(touchPosition.set(screenX, screenY, 0));

		/*
		 * Query the world for all fixtures that potentially overlap the touched
		 * point.
		 */
		world.QueryAABB(queryCallback, touchPosition.x, touchPosition.y, touchPosition.x, touchPosition.y);

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		/* Whether the input was processed */
		boolean processed = false;

		/* If a MouseJoint is defined, destroy it */
		if (mouseJoint != null) {
			if (scored) {
				if (controller) {
					world.destroyJoint(mouseJoint);
				}
				System.err.println("IFffFFFFFFFFFFFFFFFFFFFFF");
			} else {
				System.err.println("ELSEEEEEEEEEEEEEEEEEE");
				world.destroyJoint(mouseJoint);
			}

			mouseJoint = null;
			processed = true;
		}

		return processed;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		/* Whether the input was processed */
		boolean processed = false;

		/*
		 * If a MouseJoint is defined, update its target with current position.
		 */
		if (mouseJoint != null) {

			/* Translate camera point to world point */
			camera.unproject(touchPosition.set(screenX, screenY, 0));
			if (clickedBody.getUserData() != null) {
				if (clickedBody.getUserData() instanceof Sprite) {
					Sprite sprite = (Sprite) clickedBody.getUserData();
					if (sprite.toString().equals(player.getaBody().getUserData().toString())) {
						mouseJoint.setTarget(new Vector2(touchPosition.x, touchPosition.y));
					}
				}
			}

		}

		return processed;
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	public void checkCollision() {
		Body body = (Body) player.getaBody();
		if (scored == false) {
			if (body.getPosition().y < -16f) {
				if (controller) {
					controller = false;
				}
				goalSound.play();
				body.setUserData(null);
				scored = true;
				body.getWorld().destroyBody(body);
				oppositeScore++;
				opsScore.setUserData(loadSprites("oppositeScore"));

				Timer.schedule(new Task() {

					@Override
					public void run() {
						scored = false;
						player.getaBody().setUserData(loadSprites("player"));
					}
				}, 2.5f);
				Timer.schedule(new Task() {
					@Override
					public void run() {
						player.initEntity();
					}
				}, 0.1f);
			}
			if (body.getPosition().y > 16f) {
				if (controller) {
					controller = false;
				}
				goalSound.play();
				body.setUserData(null);
				scored = true;
				body.getWorld().destroyBody(body);
				playerScore++;
				plyrScore.setUserData(loadSprites("playerScore"));
				Timer.schedule(new Task() {
					@Override
					public void run() {
						player.initEntity();
						scored = false;
						player.getaBody().setUserData(loadSprites("player"));

					}
				}, 2.5f);
			}
		}
	}

	public OrthographicCamera getCamera() {
		return camera;
	}
}
