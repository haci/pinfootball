package com.mobaxe.pinfootball.utils;

import com.badlogic.gdx.Gdx;

public class Constants {

	public static final int INTERSTITIAL_FREQ = 1; // Number

	public static String PLAYAGAIN = "play";
	public static String HOME = "home";
	public static String EASY = "easy";
	public static String MEDIUM = "medium";
	public static String HARD = "hard";

	private static float offset = 0.5f;
	private static float width = 20;

	public static float realWidth = Gdx.graphics.getWidth();
	public static float realHeight = Gdx.graphics.getHeight();

	public static float height = 20 * (realHeight / realWidth);

	public static float widthMeters = width / 2 - offset;
	public static float heightMeters = height / 2 - offset;

	public static float virtualWidth = 480;
	public static float virtualHeight = 720;
	
	public static float PLAYERBALLRADIUS = 1.5f;
	private float computerBallRadius = 1.5f;
	private float ballRadius = 1;
	public static float OFFSET = 0.5f;
	public static float WIDTH = 20;

	public float getComputerBallRadius() {
		return computerBallRadius;
	}

	public void setComputerBallRadius(float computerBallRadius) {
		this.computerBallRadius = computerBallRadius;
	}

	public float getBallRadius() {
		return ballRadius;
	}

	public void setBallRadius(float ballRadius) {
		this.ballRadius = ballRadius;
	}

}
