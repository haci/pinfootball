package com.mobaxe.pinfootball;

import com.badlogic.gdx.Game;

public class PinFootball extends Game {

	public static ActionResolver actionResolver;

	public PinFootball(ActionResolver actionResolver) {
		PinFootball.actionResolver = actionResolver;
	}

	@Override
	public void create() {
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.MAIN_MENU_SCREEN);
	}

	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
	}
}
