package com.mobaxe.pinfootball.entities;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.pinfootball.MyScreens;
import com.mobaxe.pinfootball.ScreenManager;
import com.mobaxe.pinfootball.helpers.AssetsLoader;

public class CustomButton extends Button {

	public static String START = "start";
	public static int clickCounter;

	private String buttonUp;
	private String buttonName;

	private Skin skin;
	private ButtonStyle style;

	private Sound buttonSound;

	public CustomButton(String buttonName, boolean isAnimatedButton) {
		this.buttonName = buttonName;
		buttonUp = buttonName + "ButtonUp";
		initSkins();
		setButtonStyles();
		buttonSound = AssetsLoader.loadSound("sounds/buttonBeepSound.mp3");
		clickListener(buttonName, isAnimatedButton);

	}

	private void clickListener(final String buttonName, final boolean isAnimatedButton) {
		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						if (isAnimatedButton) {
							buttonAnimation();
						} else {
							buttonFunction();
						}
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction() {
				buttonSound.play();
				if (buttonName.equals(START)) {
					goToGameScreen();
				}
				clickCounter = 0;
			}

			private void buttonAnimation() {
				buttonSound.play();
				addAction(Actions.sequence(Actions.moveTo(getX(), getY() - 20, 0.2f, Interpolation.sineOut),
						Actions.moveTo(getX(), getY() + 20, 0.2f, Interpolation.sineOut),
						Actions.moveTo(getX(), getY(), 0.4f, Interpolation.sineOut), Actions.run(new Runnable() {
							@Override
							public void run() {
								if (buttonName.equals(START)) {
									goToGameScreen();
								}
								clickCounter = 0;
							}
						})));
			}
		});
	}

	private void initSkins() {
		skin = new Skin();

		if (buttonName.equals(START)) {
			skin.add(buttonUp, AssetsLoader.loadTexture("mainmenuimages/playbutton.png"));
		}
	}

	public void setButtonStyles() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		setStyle(style);
	}

	private void goToGameScreen() {

		getParent().addAction(Actions.sequence(Actions.fadeOut(1.1f), Actions.run(new Runnable() {

			@Override
			public void run() {
				ScreenManager.getInstance().dispose(MyScreens.MAIN_MENU_SCREEN);
				ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
			}
		})));
	}
}
