package com.mobaxe.pinfootball.helpers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.pinfootball.utils.Constants;

public class Box2DFactory {

	public static Body createBody(World world, BodyType bodyType, FixtureDef fixtureDef, Vector2 position) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(position);

		Body body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);
		fixtureDef.shape.dispose();

		return body;
	}

	public static Shape createBoxShape(Vector2 center, float angle) {
		PolygonShape boxShape = new PolygonShape();
		boxShape.setAsBox(Constants.widthMeters, Constants.heightMeters, center, angle);

		return boxShape;
	}

	public static Shape createChainShape(Vector2[] vertices) {
		ChainShape chainShape = new ChainShape();
		chainShape.createChain(vertices);
		return chainShape;
	}

	public static Shape createCircleShape(float radius) {
		CircleShape circleShape = new CircleShape();
		circleShape.setRadius(radius);

		return circleShape;
	}

	public static Shape createPolygonShape(Vector2[] vertices) {
		PolygonShape polygonShape = new PolygonShape();
		polygonShape.set(vertices);

		return polygonShape;
	}

	public static Shape createTriangleShape() {
		PolygonShape triangleShape = new PolygonShape();
		triangleShape.set(new Vector2[] { new Vector2(-Constants.widthMeters, -Constants.heightMeters),
				new Vector2(0, Constants.heightMeters), new Vector2(Constants.widthMeters, -Constants.heightMeters) });

		return triangleShape;
	}

	public static FixtureDef createFixture(Shape shape, float density, float friction, float restitution,
			boolean isSensor) {
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.isSensor = isSensor;
		fixtureDef.density = density;
		fixtureDef.friction = friction;
		fixtureDef.restitution = restitution;

		return fixtureDef;
	}

	public static Body createScoreBoardOpposite(World world) {
		Vector2[] vertices = new Vector2[] { new Vector2(9.5f, 1), new Vector2(9.5f, 2) };

		Shape shape = createChainShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createScoreBoardPlayer(World world) {
		Vector2[] vertices = new Vector2[] { new Vector2(9.5f, -1), new Vector2(9.5f, -2) };

		Shape shape = createChainShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createInvisibleWalls(World world) {
		Vector2[] vertices = new Vector2[] { new Vector2(-Constants.widthMeters, -Constants.heightMeters),
				new Vector2(Constants.widthMeters, -Constants.heightMeters),
				new Vector2(Constants.widthMeters, Constants.heightMeters),
				new Vector2(-Constants.widthMeters, Constants.heightMeters),
				new Vector2(-Constants.widthMeters, -Constants.heightMeters) };

		Shape shape = createChainShape(vertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createLeftLine(World world) {
		Vector2[] vertices = new Vector2[] { new Vector2(-Constants.widthMeters, -Constants.heightMeters),
				new Vector2(-Constants.widthMeters, Constants.heightMeters) };

		Shape shape = createChainShape(vertices);

		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);

		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(0, 0));

	}

	public static Body createRightLine(World world) {

		Vector2[] rightLineVercites = new Vector2[] { new Vector2(0, -Constants.widthMeters + 9.5f),
				new Vector2(1 * Constants.widthMeters - 9.5f, -Constants.heightMeters - 14.5f),

		};
		Shape shape = createChainShape(rightLineVercites);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));

	}

	public static Body createHalfLine(World world) {

		Vector2[] halfLineVertices = new Vector2[] { new Vector2(0, -Constants.heightMeters),
				new Vector2(-2 * Constants.widthMeters, -Constants.heightMeters),

		};
		Shape shape = createChainShape(halfLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));
	}

	public static Body createGoalLineDown(World world) {

		Vector2[] goalLineDownVertices = new Vector2[] { new Vector2(-8, -Constants.heightMeters - 15f),
				new Vector2(-1.2f * Constants.widthMeters, -Constants.heightMeters - 15f),

		};
		Shape shape = createChainShape(goalLineDownVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));
	}

	public static Body createGoalLineUp(World world) {

		Vector2[] goalLineUpVertices = new Vector2[] { new Vector2(-8, -Constants.heightMeters + 15f),
				new Vector2(-1.2f * Constants.widthMeters, -Constants.heightMeters + 15f),

		};
		Shape shape = createChainShape(goalLineUpVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = -1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));
	}

	public static Body createGoalLineRight(World world) {

		Vector2[] rightLineVertices = new Vector2[] {
				new Vector2(0, -Constants.heightMeters - 14.5f),
				new Vector2(-2 * Constants.widthMeters + 13, -Constants.heightMeters - 14.5f),

		};
		Shape shape = createChainShape(rightLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));
	}

	public static Body createGoalLineLeft(World world) {

		Vector2[] LeftLineVertices = new Vector2[] {

		new Vector2(-13, -Constants.heightMeters - 14.5f),
				new Vector2(-2 * Constants.widthMeters, -Constants.heightMeters - 14.5f),

		};
		Shape shape = createChainShape(LeftLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));
	}

	public static Body createGoalLineRightUp(World world) {

		Vector2[] upRightLineVertices = new Vector2[] { new Vector2(0, -Constants.heightMeters + 14.5f),
				new Vector2(-2 * Constants.widthMeters + 13, -Constants.heightMeters + 14.5f),

		};
		Shape shape = createChainShape(upRightLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));
	}

	public static Body createGoalLineLeftUp(World world) {

		Vector2[] UpLeftLineVertices = new Vector2[] {

		new Vector2(-13, -Constants.heightMeters + 14.5f),
				new Vector2(-2 * Constants.widthMeters, -Constants.heightMeters + 14.5f), };
		Shape shape = createChainShape(UpLeftLineVertices);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, new Vector2(Constants.widthMeters,
				Constants.heightMeters));
	}

	public static Body createPins(World world, Vector2 startPosition) {

		Shape shape = createCircleShape(0.5f);
		FixtureDef fixtureDef = createFixture(shape, 1, 0.5f, 0, false);
		fixtureDef.filter.groupIndex = 1;
		return createBody(world, BodyType.StaticBody, fixtureDef, startPosition);
	}

}
