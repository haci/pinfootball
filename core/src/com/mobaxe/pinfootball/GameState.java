package com.mobaxe.pinfootball;

public enum GameState {

	GAMEOVER, RUNNING, PAUSE

}
